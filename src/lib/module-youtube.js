var key = "[INSERTYOUTUBEAPIKEYHERE]";

YouTube = function() {
	/**
	 *
	 *	This function returns the latest 10 videos a given channel has uploaded
	 *
	 */
	this.getLatestChannelVideos = function(channelId,callback) {

		// get the uploads playlist
		var channelRequestURL = "https://www.googleapis.com/youtube/v3/search?channelId="+channelId+"&maxResults=50&part=snippet,id&order=date&type=video&key="+key;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", channelRequestURL, true);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				if (typeof callback !== "undefined") {
					data = JSON.parse(xhr.responseText);
					data.items.sort(function(a,b) { return new Date(b.snippet.publishedAt).getTime() - new Date(a.snippet.publishedAt).getTime(); });

					// try to get

					callback(data);
				}
			}
		}
		xhr.send();
	}


	/**
	 *
	 *	This function requests all playlists that are available
	 *	for a given channel
	 *
	 */
	this.getChannelPlaylists = function(channelId,callback,pageToken) {
		var YouTube = this;

		if (typeof callback !== "undefined") {
			var YouTube = this;

			var receivedItemsCount = 0;
			var playlists = [];

			getThoseItems(channelId,callback);

			// we have to define this functions, so we can keep calling it until all playlists were received
			function getThoseItems(channelId,callback,pageToken) {
				var playlistsRequest = "https://www.googleapis.com/youtube/v3/playlists?part=snippet,contentDetails%2Cid&channelId="+channelId+"&key="+key+"&maxResults=50";

				if (typeof pageToken !== "undefined") {
					playlistsRequest = playlistsRequest + "&pageToken="+pageToken;
				}


				var xhr = new XMLHttpRequest();
				xhr.open("GET", playlistsRequest, true);
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4) {
						if (typeof callback !== "undefined") {
							data = JSON.parse(xhr.responseText);
							//data.items.sort(function(a,b) { return new Date(b.snippet.publishedAt).getTime() - new Date(a.snippet.publishedAt).getTime(); });
							// callback(data);

							var playlistsData = data;
							if (typeof playlistsData.items !== "undefined") {
								playlistsData.items.forEach(function(playlist) {
									// count up the received items counter so we can stop once we received all items
									receivedItemsCount = receivedItemsCount + 1;
									playlists.push(playlist);
								});

								// call this function until all videos in this playlist were received
								if (typeof playlistsData.nextPageToken !== "undefined" && receivedItemsCount < playlistsData.pageInfo.totalResults) {
									getThoseItems(channelId,callback,playlistsData.nextPageToken)
								}
								else {
									// fire the callback after all playlist items were received
									// debugLog("calling back");
									callback(playlists);
								}
							}
							else {
								// debugLog(playlistsData);
							}
						}
					}
				}
				xhr.send();
			}
		}
	}


	/**
	 *
	 *	This function generates and updates a cache containing all playlists
	 *	of a given channel
	 *
	 */


	this.updatePlaylistsCaches = function(channelIds,callback) {
		// determine which channel's playlist should be requested
		var channels = [];
		if (channelIds instanceof Array) {
			channelIds.forEach(function(channelId) {
				channels.push(channelId);
			});
		}
		else {
			if (channelIds instanceof String) {
				channels.push(channelIds);
			}
		}

		var youtube = this;

		var newChannelCaches = [];

		console.log("updating playlists cache");

		Promise.each(channels,function(channelId) {
			return new Promise(function(resolve,reject) {

			// playlisten per youtube api call holen
			var newChannelCache = {
				channelId: channelId,
				playlists: []
			}
			var cpProm = new Promise(function(resolve,reject) {
				youtube.getChannelPlaylists(channelId,function(playlists) {
					newChannelCache.playlists = playlists;
					newChannelCaches.push(newChannelCache);
					console.log(newChannelCache);

					resolve();
				});
			});
			cpProm.then(function() {
				resolve();
			});
		});
		}).then(values => {

			// neue caches speichern
			chrome.storage.local.set({"playlistsCache":newChannelCaches},function() {
				console.log(newChannelCaches);
				console.log("caches updated");
			});

		});
	}
}
