moment.locale("de");

/* =============================================
				Application Logic
   ============================================= */

// - Window-Manager iniitieren -
// Der Windowmanager (/lib/windowmanager.js) ist das Skript, dass die Templates lädt
// und eben das Window Management des Addons übernimmt
var wm = new WindowManager();
$(document).ready(function() {
	wm.initiate($("#main-inner"));
});

// - Sidebar-Menü initieeren - */
// Das Menü an der Seite mit Funktionen versehen, die die notwendigen Schritte einleiten,
// um die entsprechende Seite zu öffnen, wenn der Benutzer auf einen Menü-Eintrag klickt

$(document).ready(function() {
	
	// Wenn auf Menü-Einträge geklickt wird, starte die Seiten-Lade-Prozedur (wird im background.js am Anfang erklärt)
	$("#mainmenu li,#bm-info").on("click",function() {
		var menuItem = $(this);
		var target = $(this).attr("data-switcher-target");
		if (target != "") {

			// seite öffnen (die entsprechende funktion ist etwas weiter unten in dieser Datei definiert)
			openPage(target);
		}
	});

	// Wenn auf das Unterseiten-Menü in einer Seite geklickt wird, starten wir die Seiten-Lade-Prozedur für diese Unterseite
	$("body").on("click",".window.active .window-menu li",function() {
		var menuItem = $(this);
		var target = $(this).attr("data-switcher-target");
		var parent = $(this).attr("data-switcher-parent");

		openPage(parent,target);
	});
});



// - Seite beim Startup -
// Beim Öffnen des Popup wird eine Seite geöffnet. Hier wird definiert, welche das ist
// Traditionell ist dies die Seite "Sendeplan"
$(document).ready(function() {
	openPage("sendeplan");
});


// - Externe Links -
// Externe Links sollen in einem neuen Tab geöffnet werden
$(document).ready(function() {
	$("body").on("click","a",function(e) {
		e.preventDefault();
		if (!$(this).hasClass("internal-link")) {
			// open a new tab with that url
			chrome.tabs.create({
				url: $(this).attr('href')
			});

			// close the popup
			window.close();
		}
	});
});



// - Weitere Button-Funktionen -
// Send the signal for updating the schedule cache if the user presses the corresponding button
$(document).ready(function() {
	$("body").on("click.update-sendeplan-button","#sendeplan-update-cache",function() {
		chrome.runtime.sendMessage({
			title:"updateScheduleCache"
		});
	});
});
// activate clear caches button
$(document).ready(function() {
	$("body").on("click.clear-cache-trigger",".clear-cache-trigger",function(e) {
		chrome.runtime.sendMessage({
			title:"clearCaches"
		});
	});
});
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.title == "cachesCleared") {
		alert("Cache wurde geleert");
	}
});



// - Weitere UI-Funktionen -
// Reload the schedule view after the schedule cache was updated
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.title == "scheduleCacheUpdated") {
		if ($(".window#sendeplan").hasClass("active")) {
			openPage("sendeplan");
		}
	}
});



/*  --------------------------
		Pages
	-------------------------- */


// - Seite öffnen -
// Diese Funktion wird dazu benutzt, eine Seite zu öffnen.
// Sie sendet jeweils zwei Requests an das background.js
function openPage(target,subpage,parameters) {
	if (typeof subpage !== "undefined" && subpage !== false) {

		// Der erste Request ist der 'openPage'-Request. Hier sagt das popup.js
		// im Prinzip dem background.js, dass es die Informationen wie Seiten-Titel
		// und die vorhandenen Untermenüs der angefragten Seite an das Popup schicken soll
		// Das Background.js schickt dann bei Erhalt dieses Requests, einen 'startOpeningPage'
		// Request zurück, der weiter unten verarbeitet wird (dieser startet den Aufbau der Seite)
		// Was passiert, wenn dieser Request zurück an das popup.js geht, ist unten im Listener
		// für 'startOpeningPage' definiert
		chrome.runtime.sendMessage({
			"title":"openPage",
			"page":target,
			"subpage":subpage,
			"parameters": parameters
		});

		// Gleichzeitig wird ein zweiter Request an das background.js geschickt, dass dieses doch
		// auch die dazugehörigen Daten der jeweiligen Seite laden soll. Sobald diese Daten geladen
		// wurden, werden auch diese zurück an das popup.js geschickt und es werden die Funktionen
		// ausgeführt, die diese Daten dann darstellen.
		// Diese Funktionen werden weiter unten im Listener für 'sendPageData' definiert.
		chrome.runtime.sendMessage({
			"title":"requestPageData",
			"page":target,
			"subpage":subpage,
			"parameters": parameters
		});
	}
	else {

		// dies sind die gleichen funktionen wie die beiden oben, nur für den fall, dass keine unterseiten definiert sind
		chrome.runtime.sendMessage({
			"title":"openPage",
			"page":target,
			"parameters": parameters
		});
		chrome.runtime.sendMessage({
			"title":"requestPageData",
			"page":target,
			"parameters": parameters
		});
	}

	// während die daten geladen werden, wird die lade-anzeige angezeigt
	$("#loading",this.target).addClass("active");
}


// Wenn das background.js den Request "openPage" erhält, holt es die Informationen zu der Seite
// (nur Informationen wie Seitenname und Unterseiten, noch nicht die Daten!) und sendet diese zurück.
// Diese Funktione stellt dann schon mal das Grundgerüst der Seite da (Seiten-Titel etc.).
chrome.runtime.onMessage.addListener(function(response, sender, sendResponse) {
	if (response.title == "startOpeningPage" && typeof response.pageInfo !== "undefined") {
		if (typeof response.subpage !== "undefined") {
			wm.openPage(response.pageInfo,response.subpage);
		}
		else {
			wm.openPage(response.pageInfo);
		}

	}
});





/* ---------------------------------------------------------------- *\

		DARSTELLUNGS-FUNKTIONEN FÜR ERHALTENE DATEN
		(Reaktion auf 'sendPageData'-Request vom background.js)

\* ----------------------------------------------------------------- */


// Diese Funktionen sind die Funktionen, die die vom background.js geschickten
// Daten entgegennehmen, verarbeiten und anschließend in das Popup.js einsetzen,
// sodass diese angezeigt werden.

chrome.runtime.onMessage.addListener(function(response, sender, sendResponse) {
	if (response.title == "sendPageData" && typeof response.page !== "undefined") {
		var pageInfo = response.pageInfo;



		/* ------------------------------------- *\
				Seite: VIDEO ON DEMAND
		\* ------------------------------------- */
		if (response.page == "vod") {	
			if (typeof response.data.videos.items !== "undefined") {
				var videos = response.data.videos.items;

				pageInfo = response.pageInfo;

				var bodyTemplate = wm.loadTemplate("template-default.html");
				bodyTemplate = $(bodyTemplate);

				var videoList = buildVideoList(videos);

				$(bodyTemplate).append(videoList);
				$(".template",bodyTemplate).hide();

				$(".window#"+pageInfo.page,wm.target).append(bodyTemplate);
				$(".window#"+pageInfo.page+" .window-top-button a",wm.target).attr("href","https://youtube.com/channel/"+response.data.channelId);
			}

			$(".window#"+pageInfo.page).attr("class", $(".window#"+pageInfo.page).attr("class"));

			$("#loading",wm.target).removeClass("active");
		}

		




		/* ------------------------------------- *\
				Seite: Sendeplan
		\* ------------------------------------- */
		if (response.page == "sendeplan") {
			var bodyTemplate = wm.loadTemplate("template-default.html");
			bodyTemplate = $(bodyTemplate);

			var template = wm.loadTemplate("template-sendeplan.html");
			template = $(template);

			var itemTemplate = $(".event.template",template).clone();
			itemTemplate[0].classList.remove("template");
			$(".event.template",template).remove();


			// -- Iterate through all events -- //
			var now = new Date();
			var requestedDate = new Date(response.range[0]);

			if (typeof response.data !== "undefined") {
				if (response.data instanceof Array && response.data.length > 0) {
					response.data.forEach(function(event) {
						var itemBody = $(itemTemplate).clone();

						// check if this event lies in the past
						var isPast = false;
						if (new Date(event.timeEnd) <= now) {
							isPast = true;
							$(itemBody).addClass("past");
						}

						// check if this event is the current event
						var isCurrent = false;
						if (new Date(event.timeStart) <= now && new Date(event.timeEnd) >= now) {
							$(itemBody).addClass("current");
							isCurrent = true;
						}

						// Insert Title, Topic & Game Info
						if (typeof event.title !== "undefined" && event.title != "") {
							$(".info .title",itemBody).text(event.title);
						}
						else { $(".info .title",itemBody).remove(); }

						if (typeof event.topic !== "undefined" && event.topic != "") {
							$(".info .topic",itemBody).text(event.topic);
						}
						else { $(".info .topic",itemBody).remove(); }
						
						if (typeof event.game !== "undefined" && event.game != "") {
							$(".info .game .label",itemBody).text(event.game);
						}
						else {
							$(".info .game",itemBody).remove();	
						}

						// Insert event type
						if (typeof event.type !== "undefined") {
							if (event.type == "premiere") {
								itemBody[0].classList.add("premiere");
								$(".live-status",itemBody)[0].classList.add("premiere");
								$(".live-status",itemBody).text("Neu");
							}
							if (event.type == "live") {
								itemBody[0].classList.add("live");
								$(".live-status",itemBody)[0].classList.add("live");
								$(".live-status",itemBody).text("Live");
							}
						}
						$("#sendeplan",template).append(itemBody);

						// insert times
						if (typeof event.timeStart === "string") {
							$(".meta .time .time-start",itemBody).text(moment(event.timeStart).format("HH:mm"));

							if (isCurrent) {
								$(".meta .time .time-bis-label",itemBody).text("—");
								$(".meta .time .time-end",itemBody).text(moment(event.timeEnd).format("HH:mm"));
							}
						}

						// set progress bar and remaining time
						if (isCurrent) {
							$(".progress",itemBody).css("width",timeProgress(event.timeStart,event.timeEnd) + "%");
							$(".time-remaining",itemBody).html( "läuft noch <strong>" + moment.duration(timeRemaining(event.timeEnd)).humanize() + "</strong>" );
						}


					});
					$(".no-schedule-date",template).remove();
				}
				else {
					$(".no-schedule-date .date",template).html("den <span class='dated'>" + moment(requestedDate).format("Do MMM YYYY")+"</span>");
				}
			}

			// remove all but the last three past events if this is today's schedule
			$(".event.past:lt("+($(".event.past").length-3)+")",template).hide();

			// set day class & date
			var dayStrings = ["sunday","monday","tuesday","wednesday","thursday","friday","saturday","sunday"];
			$(".schedule",template).addClass(dayStrings[requestedDate.getDay()]);
			$(".sendeplan-header .current-date .day",template).text(moment(requestedDate).format("dddd"));
			$(".sendeplan-header .current-date .date",template).text(moment(requestedDate).format("Do MMM YYYY"));


			// add click events to the day navigation
			$(template).on("click",".load-previous-day",function(){
				var yesterday = new Date(response.range[0]);
				yesterday.setDate( yesterday.getDate() - 1 );
				openPage("sendeplan",false,{"date": yesterday.getTime()})
			});
			$(template).on("click",".load-next-day",function(){
				var tomorrow = new Date(response.range[0]);
				tomorrow.setDate( tomorrow.getDate() + 1 );
				openPage("sendeplan",false,{"date": tomorrow.getTime()})
			});

			// add link to the screenshot
			$(".screenshot-wrapper",template).attr("href","https://gaming.youtube.com/user/rocketbeanstv/live");

			// set last cache check info
			if (typeof response.updated !== "undefined") {
				if (response.updated != false) {
					$(".last-updated-time",template).text(moment(new Date(response.updated)).fromNow(true));
				}
			}

			$(bodyTemplate).append(template);
			$(".window#"+pageInfo.page,wm.target).append(bodyTemplate);

			$("#loading",wm.target).removeClass("active");

			// scroll to the top of the window
			$(".window#"+pageInfo.page+" .window-content",wm.target).scrollTop(0);

			// now request the current show info to be injected
			chrome.runtime.sendMessage({
				title: "requestPageDataInjection",
				action: "getCurrentShow",
				page: "sendeplan"
			});
		}







		/* ------------------------------------- *\
				Seite: BLOG
		\* ------------------------------------- */
		if (response.page == "blog") {
			var bodyTemplate = wm.loadTemplate("template-default.html");
			bodyTemplate = $(bodyTemplate);

			var template = wm.loadTemplate("template-blog.html");
			template = $(template);

			var itemTemplate = $("article.template",template).clone();
			$(itemTemplate).removeClass("template");
			$("article.template",template).remove();

			if (typeof response.data !== "undefined") {
				response.data.forEach(function(item) {
					var itemBody = $(itemTemplate).clone();

					var pubDate = new Date(item.pubDate);
					var dateString = pubDate.getDate() + ". " + monthStrings[pubDate.getMonth()] + " " + pubDate.getFullYear();

					$(".post-title",itemBody).text( item.title );
					$(".post-date",itemBody).text( dateString );
					$(".post-link",itemBody).attr("href",item.link);
					$(".post-content",itemBody).text( $("<p>"+item.description+"</p>").text() );

					if (item.pic !== false) {
						$(".post-pic",itemBody).show();
						$(".post-pic img",itemBody).attr("src",item.pic);
					}
					else {
						$(".post-pic",itemBody).hide();
					}

					$(template).append(itemBody);
				});

				$(bodyTemplate).append(template);
				$(".window#"+pageInfo.page,wm.target).append(bodyTemplate);
			}

			$("#loading",wm.target).removeClass("active");
		}


		


		/* ------------------------------------- *\
				Seite: SUPPORT
		\* ------------------------------------- */
		if (response.page == "support") {
			var bodyTemplate = wm.loadTemplate("template-support.html");
			bodyTemplate = $(bodyTemplate);

			var template = wm.loadTemplate("templatepart-linklist.html");
			template = $(template);

			var categoryTemplate = $(".link-category.template",template).clone();
			$(categoryTemplate).removeClass("template");
			$("article.template",categoryTemplate).remove();

			var linkTemplate = $("article.template.link",template).clone();
			$(linkTemplate).removeClass("template");

			var textTemplate = $("article.template.text",template).clone();
			$(textTemplate).removeClass("template");

			$(".template",template).remove();

			if (typeof response.data !== "undefined") {
				response.data.forEach(function(category) {
					var categoryBody = $(categoryTemplate).clone();
					$(".category-title",categoryBody).text( category.categoryName );

					category.links.forEach(function(link) {
						// determine wether this is a link or a text item
						var type = "link";
						if (typeof link.url == "undefined" && typeof link.html !== "undefined") { type = "text"; }

						if (type == "link") {
							var linkBody = $(linkTemplate).clone();
						}
						else {
							var linkBody = $(textTemplate).clone();
						}

						if ($(linkBody).length > 0) {
							$(".post-title",linkBody).text( link.title );

							if (typeof link.icon === "undefined" || link.icon === "") { link.icon = "pictos/link.svg"; }
							$(".post-icon img",linkBody).attr("src", chrome.extension.getURL("data/assets/"+link.icon));

							if (type == "link") { $(".post-link",linkBody).attr("href", link.url ); }
							if (type == "text") { $(".post-content",linkBody).html(link.html ); }

							$(".post-list",categoryBody).append(linkBody);
						}
					});

					$(template).append(categoryBody);
					//itemBody = $(itemTemplate).clone();
					//$(template).append(itemBody);
				});

				$(bodyTemplate).append(template);
				
				$(".window#"+pageInfo.page,wm.target).append(bodyTemplate);
				
				setTimeout(function() {
					$("#loading",wm.target).removeClass("active");
					$(".window#"+pageInfo.page,wm.target).append(bodyTemplate);
				},1);
			}
		}

		



		/* ------------------------------------- *\
				Seite: INFO
		\* ------------------------------------- */
		if (response.page == "info") {
			setTimeout(function() {
				var template = wm.loadTemplate("template-info.html");
				template = $(template);
				$(".version strong",template).text(pageInfo.version);
				$(".window#"+pageInfo.page,wm.target).append(template);
				$("#loading",wm.target).removeClass("active");
			},50);
		}

		



		/* ------------------------------------- *\
				Seite: COMMUNITES
		\* ------------------------------------- */
		if (response.page == "communities") {
			var bodyTemplate = wm.loadTemplate("template-communities.html");
			bodyTemplate = $(bodyTemplate);

			var template = wm.loadTemplate("templatepart-linklist.html");
			template = $(template);

			var categoryTemplate = $(".link-category.template",template).clone();
			$(categoryTemplate).removeClass("template");
			$("article.template",categoryTemplate).remove();

			var itemTemplate = $("article.template.link",template).clone();
			$(itemTemplate).removeClass("template");

			$(".template",template).remove();

			if (typeof response.data !== "undefined") {
				response.data.forEach(function(category) {
					var categoryBody = $(categoryTemplate).clone();
					$(".category-title",categoryBody).text( category.categoryName );

					category.links.forEach(function(link) {
						var linkBody = $(itemTemplate).clone();
						$(".post-title",linkBody).text( link.title );
						$(".post-link",linkBody).attr("href", link.url );
						if (typeof link.icon === "undefined" || link.icon === "") { link.icon = "pictos/share.svg"; }
						$(".post-icon img",linkBody).attr("src", chrome.extension.getURL("data/assets/"+link.icon));
						$(".post-list",categoryBody).append(linkBody);
					});

					$(template).append(categoryBody);
					//itemBody = $(itemTemplate).clone();
					//$(template).append(itemBody);
				});

				$(bodyTemplate).append(template);
				
				$(".window#"+pageInfo.page,wm.target).append(bodyTemplate);
				
				setTimeout(function() {
					$("#loading",wm.target).removeClass("active");
					$(".window#"+pageInfo.page,wm.target).append(bodyTemplate);
				},1);
			}
		}

		



		/* ------------------------------------- *\
				Seite: REDDIT
		\* ------------------------------------- */
		if (response.page == "reddit") {
			var bodyTemplate = wm.loadTemplate("template-default.html");
			bodyTemplate = $(bodyTemplate);

			var template = wm.loadTemplate("template-reddit.html");
			template = $(template);

			var itemTemplate = $("article.template",template).clone();
			$(itemTemplate).removeClass("template");
			$("article.template",template).remove();

			if (typeof response.data !== "undefined") {
				response.data.forEach(function(item) {
					var itemBody = $(itemTemplate).clone();

					$(".post-link",itemBody).attr("href","https://reddit.com"+item.data.permalink);
					$(".post-title",itemBody).text(item.data.title);

					$(".post-meta .author",itemBody).text(item.data.author);
					$(".post-meta .upvotes span",itemBody).text(item.data.ups);
					$(".post-meta .comments span",itemBody).text(item.data.num_comments);

					if (item.data.link_flair_text !== null) { $(".post-meta .flair",itemBody).text(item.data.link_flair_text); }
					else { $(".post-meta .flair",itemBody).remove(); }

					if (item.data.thumbnail !== "self" && item.data.thumbnail !== "default") {
						$(".post-pic",itemBody).html('<img src="'+item.data.thumbnail+'" />');
					}
					else { $(".post-pic",itemBody).remove(); }

					var time = timeDifference(new Date(),(item.data.created_utc*1000));
					$(".post-meta .time span",itemBody).text(time);

					$(template).append(itemBody);
				});

				$(bodyTemplate).append(template);
				$(".window#"+pageInfo.page,wm.target).append(bodyTemplate);
			}

			$("#loading",wm.target).removeClass("active");
		}






		/* ------------------------------------- *\
				Seite: PLAYLISTS
		\* ------------------------------------- */
		if (response.page == "playlists") {
			
			var template = wm.loadTemplate("template-playlists.html");
			template = $(template);

			var itemTemplate = $(".playlist.template",template).clone();
			$(itemTemplate).removeClass("template");
			$(".playlist.template",template).remove();

			console.log(response);

			if (typeof response.data !== "undefined") {
				console.log(response.data);
				var playlists = response.data;

				playlists.forEach(function(playlist) {
					var item = $(itemTemplate).clone();

					$(".title",item).text(playlist.snippet.title);
					$(".video-count",item).text(playlist.contentDetails.itemCount + " Videos");
					$(".last-updated",item).hide();

					if (typeof playlist.snippet.publishedAt !== "undefined") {
						$(".created-at",item).text( "erstellt " + moment(playlist.snippet.publishedAt).fromNow() );
					}
					else {
						$(".created-at",item).hide();
					}

					var pic = $("<img>");
					$(pic).attr("src",playlist.snippet.thumbnails.medium.url);
					$(".pic-container",item).append(pic);


					if (typeof playlist.id !== "undefined") {
						$("a",item).attr("href","https://www.youtube.com/playlist?list="+playlist.id);
					}
					
					$(".playlist-list",template).append(item);
				});
			}
			
			$(".window#"+pageInfo.page,wm.target).append(template);
			$("#loading",wm.target).removeClass("active");
		}
	}


	// show a scrollbar if the content is taller than the popup
	if ($("#main").length > 0) {
		if (typeof $("#main")[0] !== "undefined") {
			if ($(window).height() < $("#main")[0].scrollHeight) {
				$("#main").css("overflow-y","scroll");
			}
			else {
				$("#main").css("overflow-y","auto");
			}
		}
	}
});



/* -- Page Data Injections Handling -- */
/* Handles data injection calls that add data to a page after it's loaded */

chrome.runtime.onMessage.addListener(function(response, sender, sendResponse) {
	if (response.title == "injectPageData") {

		/* -- Special Injections -- */
		if (typeof response.special !== "undefined" && typeof response.data !== "undefined") {

			/* - Stream Status - */
			if (response.special == "streamStatus" && response.page == "sendeplan") {
				var targetWindow = $(".window#"+response.page);
				
				var streamOfflinePic = chrome.extension.getURL("data/assets/images/stream-offline.jpg");
				$(".twitch-header .screenshot img",targetWindow).attr("src",streamOfflinePic);
				$(".twitch-header .background-pic",targetWindow).css("backgroundImage","url('"+streamOfflinePic+"')");

				$(".twitch-header .now-playing-label",targetWindow).text("");
				$(".twitch-header .now-playing-label",targetWindow).text("");
				
				$(".twitch-header .live-status",targetWindow).text("Offline");
				$(".twitch-header .live-status",targetWindow).addClass("offline");

				$(".twitch-header .info span",targetWindow).hide();
				$(".twitch-header .info .title",targetWindow).show().text("Stream ist offline…");
			}

		}
		
		/* -- Page Injection -- */
		if (typeof response.page !== "undefined" && typeof response.data !== "undefined" && typeof response.special == "undefined") {
			var action = "text";
			if (typeof response.action !== "undefined") { action = response.action; }


			/* -- Sendeplan -- */
			if (response.page == "sendeplan") {
				if (action === "text") {
					$(".window#"+response.page+" "+response.target).text(response.data);
				}
				if (action === "addClass") {
					$(".window#"+response.page+" "+response.target).addClass(response.data);
				}
			}
		

			/* -- Shows -- */
			if (response.page == "shows") {
				// determine if this show has more than playlist or not and save this information
				if (typeof response.multiple !== "undefined") { var multiple = response.multiple; }
				else { var multiple = false; }
				var videoList = buildVideoList(response.videos,"playlist",{playlistId:response.playlistId,playlistTitle:response.playlistTitle,isMultiple:multiple,playlistThumbnail:response.playlistThumbnail,videosCount:response.videos.length,lastDate:response.lastDate});

				if ($(".window#"+response.page+" .subpage-overlay",wm.target).attr("id") == "showdetail-"+response.show) {

					// spinner entfernen
					$(".window#"+response.page+" #show-detail .spinner-area .spinner",wm.target).remove();

					// remove all video lists so playlists from other shows won't be displayed if the call for the current show's playlists gets delayed or returns with an error
					if (multiple !== true && response.show !== window.lastMultipleShow) {
						$(".window#"+response.page+" #show-detail .video-list",wm.target).remove();
					}

					if (videoList !== false) {
						videoList = $(videoList);

						// $(".window#"+response.page+" #show-detail .video-list-title",wm.target).show();
						$(".window#"+response.page+" #show-detail .video-list-wrapper",wm.target).append(videoList);
					}
					else {
						// show an error message and remove the playlist section from the show detail page if there was an error with the playlist
						// $(".window#"+response.page+" #show-detail .video-list-title",wm.target).hide();
					}
				}
			}
		}
	}
});




/* =============================================
				UI Stuff
   ============================================= */

// Set the target menu item to 'active' if data was successfully received
chrome.runtime.onMessage.addListener(function(response, sender, sendResponse) {
	if (response.title == "startOpeningPage" && typeof response.pageInfo !== "undefined") {
		$("#mainmenu li").removeClass("active");
		$("#bottom-menu li").removeClass("active");
		$("#mainmenu li#mm-"+response.pageInfo.page).addClass("active");

		if ($("#mainmenu li#mm-"+response.pageInfo.page).length == 0 && $("#bm-"+response.pageInfo.page).length > 0) {
			$("#bm-"+response.pageInfo.page).addClass("active");
		}

		if (typeof response.pageInfo.subpage !== "undefined") {
			$(".window-menu li").removeClass("active");
			$(".window-menu li[data-switcher-target='"+response.pageInfo.subpage+"']");
		}
	}
});

/* ---- Header ---- */
$(document).ready(function() {
	correctWindowHeader();
	correctWindowHeaderShadow();

	$(window).on("resize",function() {
		correctWindowHeader();
	});

	$("#main").on("scroll",function() {
		correctWindowHeaderShadow();
	});

	$("#main-inner").bind("DOMSubtreeModified",function() {
		correctWindowHeader();
	});
});

/* ---- Accordions ---- */
$(document).ready(function() {
	$("body").on("click",".accordion .accordion-title",function() {
		var acc = $(this).parent();

		if ($(acc).hasClass("opened")) {
			$(".accordion-content",acc).slideUp(160);
			$(acc).removeClass("opened");
		}
		else {
			$(".accordion-content",acc).slideDown(160);
			$(acc).addClass("opened");
		}
	});
});

// /* ---- Bottom Bar ---- */
// self.port.on("windowLoadedSuccessfully", function() {
// 	$(document).ready(function() {
// 		if ($(".window.active .bottombar").length > 0) {
// 			$("#main-inner").css("padding-bottom", ($(".window.active .bottombar").height()+16) + "px");
// 		}
// 		else {
// 			$("#main-inner").css("padding-bottom", 0);
// 		}
// 	});
// });


function correctWindowHeader() {
	$(".window.active .window-header").css("width", ($(window).width()-60)+"px");
	$(".window.active .window-content").css("margin-top", ($(".window.active .window-header").height())+"px");
}

function correctWindowHeaderShadow() {
	if ($("#main").scrollTop() > 22) {
		if (!$(".window.active .window-header").hasClass("shadowed")) {
			$(".window.active .window-header").addClass("shadowed")
		}
	}
	else {
		if ($(".window.active .window-header").hasClass("shadowed")) {
			$(".window.active .window-header").removeClass("shadowed")
		}
	}
}


/* =============================================
				Helper Functions
   ============================================= */

monthStrings = ["Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"];

function buildVideoList(videoItems,type,extraData) {
	if (typeof type === "undefined") { type = "channel"; }
	if (typeof extraData === "undefined") { var extraData; }

	var template = wm.loadTemplate("templatepart-videolist.html");
	template = $(template);

	var itemTemplate = $("article.template",template).clone();
	$(itemTemplate).removeClass("template");
	$(".template",template).remove();

	if (type == "playlist") {
		$("#open-channel-in-new-tab span",template).text("Playlist");

		// show category title if this videolist is going to be displayed in one view next to other playlists, or remove it

		// // remove words in the playlist title if requested
		// if (typeof extraData.playlistTitle !== "undefined") {
		// 	extraData.playlistTitle = extraData.playlistTitle.replace("Zocken mit Bohnen |","");
		// 	extraData.playlistTitle = extraData.playlistTitle.replace("| Zocken mit Bohnen","");
		// 	extraData.playlistTitle = extraData.playlistTitle.replace("Classix |","");
		// 	extraData.playlistTitle = extraData.playlistTitle.replace("| Classix","");
		// 	extraData.playlistTitle = extraData.playlistTitle.replace("Pen & Paper |","");
		// 	extraData.playlistTitle = extraData.playlistTitle.replace("| Pen & Paper","");
		// 	extraData.playlistTitle = extraData.playlistTitle.replace("Community Beef |","");
		// 	extraData.playlistTitle = extraData.playlistTitle.replace("| Community Beef","");
		// }

		// if (typeof extraData.lastDate !== "undefined" && extraData.lastDate !== false) {
		// 	var lastDate = new Date(extraData.lastDate);
		// 	var dateString = lastDate.getDate() + "." + (lastDate.getMonth()+1) + "." + lastDate.getFullYear();

		// 	 // var now = new Date();
		// 	 // var dateString = timeDifference(now.getTime(),lastDate.getTime(),"de");
		// 	$(".playlist-last-update",template).text(dateString);
		// }

		$(".playlist-title",template).text(extraData.playlistTitle);

		// show playlist thumbnail if available
		if (typeof extraData.playlistThumbnail !== "undefined" && extraData.playlistThumbnail !== false) {
			$(".playlist-thumbnail img",template).attr("src",extraData.playlistThumbnail);
		}
		else {
			$(".playlist-thumbnail",template).remove();
		}

		// display the number of videos in the playlist
		$(".playlist-items-count",template).text(extraData.videosCount);

		if (extraData.isMultiple !== true) {
			$(".category-title",template).remove();	
		}

		if (typeof extraData.playlistId !== "undefined") {
			$("#open-channel-in-new-tab a",template).attr("href","https://www.youtube.com/playlist?list="+extraData.playlistId);
		}
	}
	else {
		$(".category-title",template).remove();	
	}

	if (typeof videoItems !== "undefined" && videoItems[0] !== "undefined") {
		videoItems.forEach(function(video) {

			// check if this video is a live stream
			var isLiveVideo = false;
			if (video.snippet.liveBroadcastContent === "live") { isLiveVideo = true; }

			// don't display deleted videos
			if (video.snippet.title !== "Deleted video" && !isLiveVideo) {
				var itemBody = $(itemTemplate).clone();

				if (typeof video.snippet.thumbnails !== "undefined") {
					$(".post-pic-inner",itemBody).css("background-image", "url('"+video.snippet.thumbnails.default.url+"')");
				}

				if (typeof video.snippet.title !== "undefined") {
					$(".post-title",itemBody).text( video.snippet.title );
				}

				// add video links (and include the playlist id if possible)
				if (type == "channel") { $(".video-link",itemBody).attr("href","http://youtube.com/watch?v="+video.id.videoId ); }
				if (type == "playlist") { $(".video-link",itemBody).attr("href","http://youtube.com/watch?v="+video.snippet.resourceId.videoId+"&list="+extraData.playlistId ); }

				// add upload date
				if (typeof video.snippet.publishedAt !== "undefined") {
					var date = new Date(video.snippet.publishedAt);
					$(".post-date",itemBody).text( date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear());
				}

				// only add the video if the videoId is available
				if (typeof video.id.videoId !== "undefined" && video.id.videoId != null) {
					if (type == "playlist") {
						$(".accordion-content",template).append(itemBody);
					}
					else {
						$(template).append(itemBody);	
					}
				}
			}
		});

		if (type == "playlist" && extraData.isMultiple == true) {
			$(template).removeClass("disabled");
			$(".accordion-content",template).hide();
		}
	}
	else {
		template = "";
	}


	return template;
}


function timeDifference(current, previous, lang) {
	if (typeof lang == "undefined") { lang = "en"; }

	var msPerMinute = 60 * 1000;
	var msPerHour = msPerMinute * 60;
	var msPerDay = msPerHour * 24;
	var msPerMonth = msPerDay * 30;
	var msPerYear = msPerDay * 365;

	var elapsed = current - previous;

	if (elapsed < msPerMinute) {
		var value = Math.round(elapsed/1000);
		if (lang == "en") { return value + 's'; }
		if (lang == "de" && value !== 1) { return value + ' Sekunden'; }
		if (lang == "de") { return value + ' Sekunde'; }
	}

	else if (elapsed < msPerHour) {
		var value = Math.round(elapsed/msPerMinute);
		if (lang == "en") { return value + 'm'; }
		if (lang == "de" && value !== 1) { return value + ' Minuten'; }
		if (lang == "de") { return value + 'Minute'; }
	}

	else if (elapsed < msPerDay ) {
		var value = Math.round(elapsed/msPerHour );
		if (lang == "en") { return value + 'h'; }
		if (lang == "de" && value !== 1) { return value + ' Stunden'; }
		if (lang == "de") { return value + ' Stunde'; }
	}

	else if (elapsed < msPerMonth) {
		var value = Math.round(elapsed/msPerDay);
		if (lang == "en") { return value+ 'd'; }
		if (lang == "de" && value !== 1) { return value + ' Tagen'; }
		if (lang == "de") { return value + ' Tag'; }
	}

	else if (elapsed < msPerYear) {
		var value = Math.round(elapsed/msPerMonth);
		if (lang == "en") { return value + 'mo'; }
		if (lang == "de" && value !== 1) { return value + ' Monaten'; }
		if (lang == "de") { return value + ' Monat'; }
	}

	else {
		var value = Math.round(elapsed/msPerYear );
		if (lang == "en") { return value + 'y'; }
		if (lang == "de" && value !== 1) { return value + ' Jahren'; }
		if (lang == "de") { return value + ' Jahr'; }
	}
}


// Convert a string like "1 Std. 20 Min." to milliseconds
function timespanMillis(string) {
  var tMillis = {
    second: 1000,
    min: 60 * 1000,
    minute: 60 * 1000,
    hour: 60 * 60 * 1000 // etc.
  };

  if (typeof string == "string") {
  	var matches = string.match(/(\d+)\s*(Std|Min)/g);
  	if (matches.length > 0) {
  		var minutes = 0;
  		var hours = 0;

  		matches.forEach(function(match) {
  			if (match.indexOf(" Std") > -1) {
  				hours = parseInt( match.replace(" Std","") , 10);
  			}

  			else if (match.indexOf(" Min") > -1) {
  				minutes = parseInt( match.replace(" Min","") , 10);
  			}
  		});

  		var milliseconds = (minutes*tMillis.minute) + (hours*tMillis.hour);
  		return milliseconds;
  	}
  }
  
}


// console.log from popup to background
function log(message) {
	chrome.runtime.sendMessage({
		title: "log",
		message: message
	});
}


function timeProgress(start,end) {
	start = new Date(start);
	end = new Date(end);
	var now = new Date();

	var duration = end - start;
	var elapsed = end - now;

	return 100 - (elapsed/duration)*100;
}

function timeRemaining(end) {
	var end = new Date(end);
	var now = new Date();
	var remaining = end - now;

	return remaining;
}